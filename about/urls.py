from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView
from news.views import ProfileView, SetView, SignUpView, \
    LikedBookmarksView, ViewedBookmarksView, NotificationsView, \
    UserView, CommentLikeView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'about.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    url(r'^search/', include('haystack.urls')),

    url(r'^tinymce/', include('tinymce.urls')),

    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', 'news.views.home', name='home'),
    url(r'^set/$', SetView.as_view()),

    url(r'^profile/', ProfileView.as_view(), name='profile'),
    url(r'^sign_in/$', 'django.contrib.auth.views.login', {'template_name': 'users/sign_in.html'}, name='sign_in'),
    url(r'^sign_up/$', SignUpView.as_view(), name='sign_up'),
    url(r'^sign_out/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='sign_out'),
    url(r'^password_change/$', 'django.contrib.auth.views.password_change', {'post_change_redirect': '/profile'}),

    url(r'^user/(?P<user_id>\d+)/$', UserView.as_view(), name='user'),

    url(r'^bookmarks/$', RedirectView.as_view(url='/bookmarks/liked'), name='bookmarks'),
    url(r'^bookmarks/liked$', LikedBookmarksView.as_view(), name='bookmarks_liked'),
    url(r'^bookmarks/viewed$', ViewedBookmarksView.as_view(), name='bookmarks_viewed'),

    url(r'^notifications$', NotificationsView.as_view(), name='notifications'),

    url(r'^articles/', include('news.urls')),
    url(r'^comment/like/(\d+)/$', CommentLikeView.as_view(), name='comments_like'),
)
