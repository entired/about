
function addFilterClickHandlers() {
    var callback = function(property, element) {
        var value = element.is(":checked");
        $.ajax({
            type: "get",
            url: "/set?key=" + property + "&value=" + value
        }).done(function() {
            location.reload();
        }).fail(function() {
            element.popover("show");
            setTimeout(function() { element.popover('hide') }, 3500);
        });
    }

    $("#important-only").click(function() {
        callback("important_only", $(this));
    });
    $("#inverse").click(function() {
        callback("inverse", $(this));
    });
}

function addLikeClickHandlers() {
    $(".like-button").click(function() {
        var button = $(this);
        var action = button.hasClass("liked") ? "dislike" : "like";
        $.ajax({
            type: "get",
            url: button.data("url") + "?action=" + action,
        }).done( function(data) {
            button.toggleClass("liked");
            button.toggleClass("btn-default");
            button.toggleClass("btn-danger");
            button.find(".like-counter").text(data.like_count);
        });
    });
}


function addClickHandlers() {
    addLikeClickHandlers();
    addFilterClickHandlers();
}

function initTwitterWidget() {
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], p=/^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js,fjs);
        }
    }(document, 'script', 'twitter-wjs'));
}

function initFacebookWidget() {
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}

function initShareWidgets() {
    initTwitterWidget();
    initFacebookWidget();
}

function init() {
    addClickHandlers();
    initShareWidgets();
}

$(document).ready(init);
