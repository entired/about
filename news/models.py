import urllib, hashlib, datetime
from django.db import models
from django.db.models import Count
from django.conf import settings
from django.contrib.auth.models import User
from tinymce.models import HTMLField


class LikeMixin:
    def is_liked_by(self, user):
        return self.user_likes.filter(pk=user.id).exists()

    def get_likes_count(self):
        return self.user_likes.count()

    def like(self, user, action):
        if action == 'like' and not self.is_liked_by(user):
            self.user_likes.add(user)
        elif action == 'dislike' and self.is_liked_by(user):
            self.user_likes.remove(user)
        else:
            return False
        return True


class Category(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = "categories"

    def __unicode__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=15)

    def __unicode__(self):
        return self.name


class ArticleManager(models.Manager):
    def popular(self):
        return self.all().annotate(likes=Count('user_likes')).order_by('-likes')

    def recent(self):
        return self.all().order_by('-published')

    def most_viewed(self):
        return self.all().annotate(views=Count('user_views')).order_by('-views')

    def get_queryset(self):
        return super(ArticleManager, self).get_queryset().filter(status=1)


class Article(models.Model, LikeMixin):
    STATUS_CHOICES = (
        (0, 'Inactive'),
        (1, 'Active'),
        (2, 'Archive')
    )
    title = models.CharField(max_length=100)
    annotation = models.CharField(max_length=1000)
    body = HTMLField()
    category = models.ForeignKey(Category)
    tags = models.ManyToManyField(Tag, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    published = models.DateTimeField(default=datetime.datetime.now)
    important = models.BooleanField(default=False)
    rating = models.IntegerField(default=0)
    user_likes = models.ManyToManyField(User, related_name='liked_articles')
    user_views = models.ManyToManyField(User, related_name='viewed_articles')
    status = models.IntegerField(default=0, choices=STATUS_CHOICES)
    picture = models.ImageField(null=True, blank=True)

    default_manager = models.Manager()
    objects = ArticleManager()

    def mark_viewed(self, user):
        user_viewed = self.user_views.filter(pk=user.id)
        if not user_viewed.exists():
            self.user_views.add(user)

    def get_views_count(self):
        return self.user_views.count()

    @models.permalink
    def get_absolute_url(self):
        return ('article', [str(self.id)])

    def __unicode__(self):
        return self.title


class Message(models.Model):
    theme = models.CharField(max_length=30)
    body = models.CharField(max_length=1000)

    class Meta:
        ordering = ['theme', 'body']

    def __unicode__(self):
        return self.body


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    full_name = models.CharField(max_length=30)
    telephone = models.CharField(max_length=15, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    messages = models.ManyToManyField(Message, through="Notification", related_name="receivers")

    def gravatar_url(self, size):
        default = settings.GRAVATAR_DEFAULT
        gravatar_url = "http://www.gravatar.com/avatar/" + hashlib.md5(self.user.email.lower()).hexdigest() + "?"
        return gravatar_url + urllib.urlencode({'d': default, 's': str(size)})

    def gravatar_url_small(self):
        return self.gravatar_url(40)

    def gravatar_url_medium(self):
        return self.gravatar_url(100)

    def gravatar_url_big(self):
        return self.gravatar_url(200)

    def get_unread_messages_count(self):
        return self.messages.filter(notification__viewed=False).count()

    @models.permalink
    def get_absolute_url(self):
        return ('user', [str(self.id)])

    def __unicode__(self):
        return u'Profile of user: ' + self.user.username


class Notification(models.Model):
    user = models.ForeignKey(UserProfile, related_name='notification')
    message = models.ForeignKey(Message, related_name='notification')
    viewed = models.BooleanField(default=False)


class Comment(models.Model, LikeMixin):
    user = models.ForeignKey(UserProfile, related_name="comments", related_query_name="comment")
    article = models.ForeignKey(Article, related_name="comments", related_query_name="comment")
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    user_likes = models.ManyToManyField(User, related_name="liked_comments", blank=True)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return u'Comment on the article: ' + self.article.title

