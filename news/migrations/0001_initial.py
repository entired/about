# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('annotation', models.CharField(max_length=1000)),
                ('body', models.TextField()),
                ('category', models.ForeignKey(default=None, to_field='id', blank=True, to='news.Category', null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('important', models.BooleanField(default=False)),
                ('rating', models.IntegerField(default=0)),
                ('status', models.IntegerField(default=0, choices=[(0, b'Inactive'), (1, b'Active'), (2, b'Archive')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
