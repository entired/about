# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0010_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='telephone',
            field=models.CharField(max_length=15, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='birth_date',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
