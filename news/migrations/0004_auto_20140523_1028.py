# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('news', '0003_auto_20140523_0555'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('annotation', models.CharField(max_length=1000)),
                ('body', models.TextField()),
                ('category', models.ForeignKey(default=None, to_field='id', blank=True, to='news.Category', null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('important', models.BooleanField(default=False)),
                ('rating', models.IntegerField(default=0)),
                ('views', models.IntegerField(default=0)),
                ('status', models.IntegerField(default=0, choices=[(0, b'Inactive'), (1, b'Active'), (2, b'Archive')])),
                ('user_likes', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='Entry',
        ),
    ]
