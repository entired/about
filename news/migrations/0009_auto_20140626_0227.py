# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0008_auto_20140625_1647'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('theme', models.CharField(max_length=30)),
                ('body', models.CharField(max_length=1000)),
            ],
            options={
                'ordering': [b'theme', b'body'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('viewed', models.BooleanField(default=False)),
                ('message', models.ForeignKey(to='news.Message', to_field='id')),
                ('user', models.ForeignKey(to='news.UserProfile', to_field='id')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='messages',
            field=models.ManyToManyField(to=b'news.Message', through='news.Notification'),
            preserve_default=True,
        ),
    ]
