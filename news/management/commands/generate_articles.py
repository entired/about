
from datetime import datetime
from random import randint, sample
from re import sub
from json import dumps
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    word_count = (3, 13)
    sentence_count = (1, 7)
    paragraph_count = (1, 5)
    entry_count = 15
    model_name = 'news.article'
    source_file = 'news/management/commands/source.txt'

    def get_words(self, path):
        with open(path, 'r') as f:
            content = f.read()
        content = sub('[^a-zA-Z]+', ' ', content)
        dic = { w.lower() : None for w in content.split(' ') if len(w) > 2 }
        return dic.keys()

    def word(self):
        return self.words[randint(0, len(self.words) - 1)]

    def sentence(self):
        lst = []
        for _ in range(0, randint(*self.word_count)):
            w = self.word()
            if randint(0, 100) > 90:
                w = w + ','
            lst.append(w)
        return ' '.join(lst).capitalize().strip(',') + '.'

    def paragraph(self):
        lst = []
        for _ in range(0, randint(*self.sentence_count)):
            lst.append(self.sentence())
        return ' '.join(lst)

    def body(self):
        lst = []
        for _ in range(0, randint(*self.paragraph_count)):
            lst.append(self.paragraph())
        return '\n'.join(lst)

    def fields(self):
        f = {}
        f['title'] = self.sentence().strip('.')
        f['annotation'] = self.paragraph()
        f['body'] = self.body()
        f['category'] = randint(1, 4)
        f['tags'] = sample(range(1, 7), randint(1, 4))
        f['created'] = str(datetime.now())
        f['published'] = str(datetime.now())
        f['important'] = randint(0, 1)
        f['status'] = 1
        f['picture'] = 'pic{}.jpg'.format(randint(1, 5))
        return f

    def handle(self, *args, **options):
        self.words = self.get_words(self.source_file)
        entry_list = []
        for id in range(0, 15):
            entry = { 'fields': self.fields(), 'model': self.model_name, 'pk': id }
            entry_list.append(entry)
        print(dumps(entry_list))
