import re
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.forms import ModelForm
from .models import UserProfile, Comment
from collections import OrderedDict

class ProfileForm(ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))

    def save(self, commit=True):
        profile = super(ProfileForm, self).save(commit=False)
        user = profile.user
        user.email = self.cleaned_data['email']
        if commit:
            profile.save()
            user.save()
        return profile

    def clean_telephone(self):
        telephone = self.cleaned_data.get('telephone', None)
        if not re.match(r'^\+?\d{0,15}$', telephone):
            raise forms.ValidationError('Please enter a valid phone number')
        return telephone

    class Meta:
        model = UserProfile
        fields = ['full_name', 'telephone', 'birth_date', 'email']
        widgets = {
            'full_name': forms.TextInput(attrs={'class': 'form-control'}),
            'telephone': forms.TextInput(attrs={'class': 'form-control'}),
            'birth_date': forms.TextInput(attrs={'class': 'form-control'}),
        }


class SignUpForm(UserCreationForm):
    email = forms.EmailField()

    def save(self, commit=True):
        user = super(SignUpForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
            UserProfile(user=user).save()
        return user


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['body']
        widgets = {
            'body': forms.Textarea(attrs={'class': 'form-control', 'cols': 50, 'rows': 10}),
        }
