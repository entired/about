import json
from django.utils.decorators import method_decorator
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.generic import View
from django.core.paginator import Paginator, EmptyPage
from reportlab.pdfgen import canvas
from .models import Article, Category, UserProfile, Comment, Notification
from .forms import ProfileForm, SignUpForm, CommentForm


class PaginationMixin:
    per_page = 10
    default_page = 1

    def paginate(self, request, objects):
        page_number = int(request.GET.get('page', self.default_page))

        paginator = Paginator(objects, self.per_page)
        try:
            paginated_objects = paginator.page(page_number)
        except EmptyPage:
            paginated_objects = paginator.page(paginator.num_pages)

        return paginated_objects


def home(request):
    return redirect('articles_recent')


class BaseArticlesView(View):
    template_name = ''

    def __init__(self):
        self.context = {}

    def prepare_data(self, request, *args, **kwargs):
        pass

    def get(self, request, *args, **kwargs):
        self.context['categories'] = Category.objects.all()
        if 'category' in kwargs:
            category = get_object_or_404(Category, name=kwargs['category'])
            self.context['category'] = category

        self.prepare_data(request, *args, **kwargs)

        if 'articles' in self.context:
            for article in self.context['articles']:
                article.liked = article.is_liked_by(request.user)

        return render(request, self.template_name, self.context)


class ArticlesRecentView(BaseArticlesView, PaginationMixin):
    template_name = 'articles/index.html'
    articles_method = Article.objects.recent
    sort_method = 'recent'
    per_page = 5

    def prepare_data(self, request, *args, **kwargs):
        articles = self.articles_method()
        if 'category' in self.context:
            articles = articles.filter(category__name=self.context['category'])

        self.context['important_only'] = request.session.get('important_only', 'false')
        self.context['inverse'] = request.session.get('inverse', 'false');

        if self.context['important_only'] == 'true':
            articles = articles.filter(important=True)
        if self.context['inverse'] == 'true':
            articles = articles.reverse()

        self.context['articles'] = self.paginate(request, articles)
        self.context['sort_method'] = self.sort_method


class ArticlesPopularView(ArticlesRecentView):
    articles_method = Article.objects.popular
    sort_method = 'popular'


class ArticlesMostViewedView(ArticlesRecentView):
    articles_method = Article.objects.most_viewed
    sort_method = 'most_viewed'


class ArticlesDetailView(BaseArticlesView):
    template_name = 'articles/detail.html'

    def prepare_data(self, request, *args, **kwargs):
        article = get_object_or_404(Article, pk=kwargs['article_id'])
        comments = article.comments.all();

        if request.user.is_authenticated():
            article.mark_viewed(request.user)

        self.context['articles'] = [article]
        self.context['comments'] = comments

        for comment in self.context['comments']:
                comment.liked = comment.is_liked_by(request.user)

        self.context['form'] = CommentForm()
        self.context['related_content'] = Article.objects.recent().filter(category=article.category)[:3]


class ArticleLikeView(View):
    def get(self, request, article_id):
        if not request.user.is_authenticated():
            return HttpResponseForbidden()

        article = get_object_or_404(Article, pk=article_id)
        action = request.GET.get('action')

        if article.like(request.user, action):
            json_data = json.dumps({'like_count': article.get_likes_count()})
            return HttpResponse(json_data, content_type="application/json")
        else:
            return HttpResponseForbidden()


class ArticlePDFView(View):
    def get(self, request, article_id):
        if not request.user.is_authenticated():
            return HttpResponseForbidden()

        article = get_object_or_404(Article, pk=article_id)

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % article.title

        p = canvas.Canvas(response)
        p.drawString(50, 800, article.title)
        p.drawString(50, 750, article.body)

        p.showPage()
        p.save()
        return response


class CommentView(View):
    @method_decorator(login_required)
    def post(self, request, article_id):
        form = CommentForm(request.POST)
        if form.is_valid():
            new_comment = Comment()
            new_comment.body = form.cleaned_data['body']
            new_comment.user = request.user.profile
            new_comment.article = get_object_or_404(Article, pk=article_id)
            new_comment.save()
            messages.info(request, 'Comment is successfully added!')
        else:
            messages.error(request, 'Invalid comment!')

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class CommentLikeView(View):
    def get(self, request, comment_id):
        if not request.user.is_authenticated():
            return HttpResponseForbidden()

        comment = get_object_or_404(Comment, pk=comment_id)
        action = request.GET.get('action')

        if comment.like(request.user, action):
            json_data = json.dumps({'like_count': comment.get_likes_count()})
            return HttpResponse(json_data, content_type="application/json")
        else:
            return HttpResponseForbidden()


class SetView(View):
    def get(self, request, *args, **kwargs):
        allowed_keys = set(['important_only', 'inverse'])
        if 'key' in request.GET and 'value' in request.GET and request.GET['key'] in allowed_keys:
            request.session[request.GET['key']] = request.GET['value']
            return HttpResponse("")
        else:
            return HttpResponseForbidden()


class SignUpView(View):
    def get(self, request, *args, **kwargs):
        form = SignUpForm()
        return render(request, 'users/sign_up.html', {'form': form})

    def post(self, request, *args, **kwargs):
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.info(request, 'Registered')
            return redirect('home')
        else:
            return render(request, 'users/sign_up.html', {'form': form})


class ProfileView(View):
    @method_decorator(login_required)
    def get(self, request):
        form = ProfileForm(instance=request.user.profile, initial={'email': request.user.email})
        return render(request, 'users/profile.html', {'form': form, 'profile': request.user.profile})

    @method_decorator(login_required)
    def post(self, request):
        form = ProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()  # !
            messages.info(request, 'Profile updated')
        else:
            messages.error(request, 'Data is not valid')
        return redirect('profile')


class UserView(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_object_or_404(UserProfile, pk=kwargs['user_id'])
        return render(request, 'users/user_info.html', {'user_info': user})


class LikedBookmarksView(View, PaginationMixin):
    bookmark = 'liked'

    def __init__(self):
        self.context = {'bookmark': self.bookmark}

    def prepare_data(self, request):
        print dir(request.user.liked_articles)
        self.articles = request.user.liked_articles.all()

    def get(self, request):
        self.prepare_data(request)
        self.context['articles'] = self.paginate(request, self.articles)
        return render(request, 'bookmarks.html', self.context)


class ViewedBookmarksView(LikedBookmarksView):
    bookmark = 'viewed'

    def prepare_data(self, request):
        self.articles = request.user.viewed_articles.all()


class NotificationsView(View, PaginationMixin):
    def get(self, request):
        context = { 'notifications':  Notification.objects.filter(user=request.user.profile) }
        return render(request, 'notifications.html', context)

    def post(self, request):
        error = False
        notifications = Notification.objects.filter(user=request.user.profile)
        for key in request.POST:
            if key.startswith('message'):
                try:
                    pk = int(key.split('_')[1])
                    if request.POST[key] == 'on':
                        viewed = True
                    else:
                        viewed = False
                    notification = notifications.get(pk=pk)
                    notification.viewed = viewed
                    notification.save()
                except Exception as e:
                    messages.error(request, e.message)
                    error = True
        if error:
            messages.error(request, 'Cannot properly handle request')
        return redirect('notifications')

