from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^recent/$', views.ArticlesRecentView.as_view(), name='articles_recent'),
    url(r'^recent/(?P<category>\w*)/$', views.ArticlesRecentView.as_view(), name='articles_recent'),

    url(r'^popular/$', views.ArticlesPopularView.as_view(), name='articles_popular'),
    url(r'^popular/(?P<category>\w*)/$', views.ArticlesPopularView.as_view(), name='articles_popular'),

    url(r'^most_viewed/$', views.ArticlesMostViewedView.as_view(), name='articles_most_viewed'),
    url(r'^most_viewed/(?P<category>\w*)/$', views.ArticlesMostViewedView.as_view(), name='articles_most_viewed'),

    url(r'^(?P<article_id>\d+)/$', views.ArticlesDetailView.as_view(), name='article'),
    url(r'^(\d+)/like/$', views.ArticleLikeView.as_view(), name='articles_like'),
    url(r'^(\d+)/pdf/$', views.ArticlePDFView.as_view(), name='articles_pdf'),
    url(r'^(?P<article_id>\d+)/comment/$', views.CommentView.as_view(), name='articles_comment'),
)
