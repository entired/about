from datetime import datetime
from django.contrib import admin
from . import models


@admin.register(models.Article)
class ArticleAdmin(admin.ModelAdmin):
    view_on_site = True
    fields = ('important', 'category', 'status', 'title', 'annotation', 'body', 'picture', 'tags', 'published')
    raw_id_fields = ('tags',)
    list_display = ('title', 'category', 'status', 'important', 'created', 'published')
    actions = ('publish', 'inactivate', 'archive', 'mark_important', 'mark_not_important')
    
    def publish(self, request, queryset):
        queryset.update(published=datetime.now())
        queryset.update(status=1)

    def inactivate(self, request, queryset):
        queryset.update(status=0)

    def archive(self, request, queryset):
        queryset.update(status=2)

    def mark_important(self, request, queryset):
        queryset.update(important=True)

    def mark_not_important(self, request, queryset):
        queryset.update(important=False)

    def queryset(self, request):
        return models.Article.default_manager


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    fields = ('theme', 'body')
    actions = ('broadcast',)

    def broadcast(self, request, queryset):
        for message in queryset:
            for user in models.User.objects.all():
                if hasattr(user, 'profile'):
                    models.Notification.objects.create(user=user.profile, message=message)


admin.site.register(models.Category)
admin.site.register(models.Tag)
admin.site.register(models.UserProfile)
admin.site.register(models.Comment)

