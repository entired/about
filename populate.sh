#!/bin/sh
python manage.py generate_articles > fixtures/articles.json

python manage.py loaddata fixtures/categories.json
python manage.py loaddata fixtures/tags.json
python manage.py loaddata fixtures/articles.json

rm fixtures/articles.json
